import RPG_Heroes.*;
import RPG_Heroes.HeroAttribute;
import RPG_Heroes.Heroes.Mage;
import RPG_Heroes.Items.ArmorType;
import RPG_Heroes.Items.Slot;
import RPG_Heroes.Items.WeaponType;

public class Main {
    public static void main(String[] args) throws HerosExceptions {
        Mage mage = new Mage("Dewald");
        mage.LevelUp();
        mage.LevelUp();
        mage.LevelUp();
        mage.EquipWeapon("Psycho Wand", 1, WeaponType.Wands, 9);
        mage.EquipWeapon("Psycho Wand", 1, WeaponType.Staffs, 3);
        mage.EquipArmor("Sorting Hat", 1, ArmorType.Cloth, Slot.Head,
                new HeroAttribute(1,2,3));
        mage.EquipArmor("Blue Wizard Robe", 1, ArmorType.Cloth, Slot.Body,
                new HeroAttribute(1,2,2));
        mage.EquipArmor("Blue Skirt", 1, ArmorType.Cloth, Slot.Legs,
                new HeroAttribute(1,3,1));
        mage.Display();
    }
}