package RPG_Heroes.Heroes;
import RPG_Heroes.HeroAttribute;
import RPG_Heroes.Items.ArmorType;
import RPG_Heroes.Items.WeaponType;
import java.util.ArrayList;
public class Ranger extends Hero{
    public Ranger(String name){ // Initializing a Mage hero by giving the name and default level 1 up to its parent.
                                // Also initialization its default attributes and valid weapon and armor types
        super(name, 1);
        this.heroAttribute = new HeroAttribute(1,7,1);
        this.validWeaponTypes = new ArrayList<>();
        this.validWeaponTypes.add(WeaponType.Bows);
        this.validArmorTypes = new ArrayList<>();
        this.validArmorTypes.add(ArmorType.Leather);
        this.validArmorTypes.add(ArmorType.Mail);
    }

    @Override
    public void LevelUp() {  // When a Ranger levels up it increases 5 in dexterity, and 1 in intel and strength
        super.LevelUp();
        heroAttribute.Smarter();
        heroAttribute.Stronger();
        for (int i = 0; i < 5; i++) {
            heroAttribute.Faster();
        }
    }
}