package RPG_Heroes.Heroes;

import RPG_Heroes.HeroAttribute;
import RPG_Heroes.Items.ArmorType;
import RPG_Heroes.Items.WeaponType;

import java.util.ArrayList;

public class Warrior extends Hero{
    public Warrior(String name){ // Initializing a Mage hero by giving the name and default level 1 up to its parent.
                                 // Also initialization its default attributes and valid weapon and armor types
        super(name, 1);
        this.heroAttribute = new HeroAttribute(5,2,1);
        this.validWeaponTypes = new ArrayList<>();
        this.validWeaponTypes.add(WeaponType.Axes);
        this.validWeaponTypes.add(WeaponType.Hammers);
        this.validWeaponTypes.add(WeaponType.Swords);
        this.validArmorTypes = new ArrayList<>();
        this.validArmorTypes.add(ArmorType.Plate);
    }

    @Override
    public void LevelUp() {  // When a Warrior levels up it increases 2 in dexterity, 3 in strength and 1 in intel
        super.LevelUp();
        heroAttribute.Smarter();
        for (int i = 0; i < 2; i++) {
            heroAttribute.Faster();
        }
        for (int i = 0; i < 3; i++) {
            heroAttribute.Stronger();
        }
    }
}