package RPG_Heroes.Heroes;
import RPG_Heroes.HeroAttribute;
import RPG_Heroes.Items.ArmorType;
import RPG_Heroes.Items.WeaponType;
import java.util.ArrayList;

public class Mage extends Hero{ // Initializing a Mage hero by giving the name and default level 1 up to its parent.
                                // Also initialization its default attributes and valid weapon and armor types
    public Mage(String name){
        super(name, 1);
        this.heroAttribute = new HeroAttribute(1,1,8);
        this.validWeaponTypes = new ArrayList<>();
        this.validWeaponTypes.add(WeaponType.Wands);
        this.validWeaponTypes.add(WeaponType.Staffs);
        this.validArmorTypes = new ArrayList<>();
        this.validArmorTypes.add(ArmorType.Cloth);
    }

    @Override
    public void LevelUp() {  // When a mage levels up it increases 5 in intelligence, and 1 in dex and strength
        super.LevelUp();
        heroAttribute.Faster();
        heroAttribute.Stronger();
        for (int i = 0; i < 5; i++) {
            heroAttribute.Smarter();
        }
    }
}
