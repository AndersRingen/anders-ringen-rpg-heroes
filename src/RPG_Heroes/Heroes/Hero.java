package RPG_Heroes.Heroes;
import RPG_Heroes.*;
import RPG_Heroes.Items.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Hero implements Playable {

    private String name;// Variables that all classes share in the private mode
    private int level; // for security reasons. I use getters to get their value.
    protected HeroAttribute heroAttribute;

    protected List<WeaponType> validWeaponTypes;
    protected List<ArmorType> validArmorTypes;
    private HashMap<Slot, Item> equipment;

    public Hero(String name, int level) {
        this.name = name;
        this.level = level;
        equipment = new HashMap();
        this.equipment.put(Slot.Weapon, null); // initializing the equipment hashmap with empty slots
        this.equipment.put(Slot.Head, null);
        this.equipment.put(Slot.Body, null);
        this.equipment.put(Slot.Legs, null);
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }


    public HashMap<Slot, Item> getEquipment() {
        return equipment;
    }

    @Override
    public void LevelUp() {
        level++;
    } // increases level of the hero

    @Override // Equip a weapon by first checking if it is the right weapontype and level, then making an object of weapon and putting it into the slot of the equipment hashmap
    public void EquipWeapon(String name, int requiredLevel, WeaponType weaponType, int  weaponDamage) throws HerosExceptions {
        if (level >= requiredLevel && validWeaponTypes.contains(weaponType)) {
            equipment.put(Slot.Weapon, new Weapon(name, requiredLevel, weaponType, weaponDamage));
        } else{ // Throwing a custom exception if the criteria is not met
            throw new HerosExceptions("Oops, this did not work! Either the level of the hero is too low or this hero cannot use this weapon type.");
        }
    }

    @Override // The same as EquipWeapon(), just with Armor and other inputs
    public void EquipArmor(String name, int requiredLevel, ArmorType armorType, Slot slot, HeroAttribute armorAttributes) throws HerosExceptions { // armorAttributes
        if (level >= requiredLevel && validArmorTypes.contains(armorType)) {
            equipment.put(slot, new Armor(name, requiredLevel, armorType, slot, armorAttributes));
        } else {
            throw new HerosExceptions("Oops, this did not work! Either the level of the hero is too low or this hero cannot use this armor type.");
        }
    }

    public float Damage() { // Calculating the damage by getting the total attributes of the hero
        List<Float> liste = TotalAttributes(); // and the damage value of the weapon.
        float damage = 0;
        if (equipment.get(Slot.Weapon) == null){  // WeaponDamage * (1 + DamagingAttribute/100).
            damage = 1;                           // With the damaging attribute differing from the classes.
        } else if(this.getClass().getSimpleName().toString().equals("Mage")) {
            damage = ((Weapon)equipment.get(Slot.Weapon)).getWeaponDamage() * (1+ liste.get(2)/100);
        } else if(this.getClass().getSimpleName().toString().equals("Ranger")) {
            damage = ((Weapon)equipment.get(Slot.Weapon)).getWeaponDamage() * (1+ liste.get(1)/100);
        } else if(this.getClass().getSimpleName().toString().equals("Rogue")) {
            damage = ((Weapon)equipment.get(Slot.Weapon)).getWeaponDamage() * (1+ liste.get(1)/100);
        } else if(this.getClass().getSimpleName().toString().equals("Warrior")) {
            damage = ((Weapon)equipment.get(Slot.Weapon)).getWeaponDamage() * (1+ liste.get(0)/100);
        }
        return damage;
    }

    public List<Float> TotalAttributes() {
        List<Float> totalAttributes= new ArrayList<Float>();
        float str = heroAttribute.getStrength();      // Calculating the total attributes of the hero
        float dex = heroAttribute.getDexterity();     // by getting its hero attributes and adding
        float intel = heroAttribute.getIntelligence();// the armor attributes

        for (Map.Entry<Slot,Item> set: equipment.entrySet()) { // here looping through the hashmap of
            if (set.getKey() != Slot.Weapon && set.getValue()!=null){ // the hero's equipment
                str += ((Armor)set.getValue()).getArmorAttribute().getStrength();
                dex += ((Armor)set.getValue()).getArmorAttribute().getDexterity();
                intel += ((Armor)set.getValue()).getArmorAttribute().getIntelligence();
            }
        }
        totalAttributes.add(str);
        totalAttributes.add(dex);
        totalAttributes.add(intel);
        return totalAttributes;
    }

    @Override
    public String Display() { // Retrieving and displaying the stats of the hero into one string
        StringBuilder string = new StringBuilder();
        List<Float> liste = TotalAttributes();
        string.append("Name: "+name+"\nClass: "+ this.getClass().getSimpleName()
                +"\nLevel: "+level
                +"\nStrength: "+Math.round(liste.get(0))
                +"\nDexterity: "+Math.round(liste.get(1))
                +"\nIntelligence: "+Math.round(liste.get(2))
                +"\nDamage: "+Damage());
        System.out.println(string);
        return string.toString();
    }
}