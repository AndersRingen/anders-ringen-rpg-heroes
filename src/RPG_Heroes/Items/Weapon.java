package RPG_Heroes.Items;

public class Weapon extends Item{

    private WeaponType weaponType;

    private int weaponDamage;

    public Weapon(String name, int requiredLevel, WeaponType weaponType, int weaponDamage) {
        super(name, requiredLevel, Slot.Weapon);
        this.weaponType = weaponType;    // The Weapon extends Item with its own weapon type and its damage
        this.weaponDamage = weaponDamage;
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }
    public int getWeaponDamage() {
        return weaponDamage;
    }
}
