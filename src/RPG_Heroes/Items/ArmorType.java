package RPG_Heroes.Items;

public enum ArmorType { // The different types of Armor
    Cloth, Leather, Mail, Plate
}
