package RPG_Heroes.Items;

public abstract class Item {
    private String name;
    private int requiredLevel;
    private Slot slot;
    public Item(String name, int requiredLevel, Slot slot) {
        this.name = name;
        this.requiredLevel = requiredLevel; // The parent class of weapon and armor.
        this.slot = slot;                   // It stores where on the body the equipment is
    }                                       // and its required level and name
    public String getName() {
        return name;
    }
    public int getRequiredLevel() {
        return requiredLevel;
    }
    public Slot getSlot() {
        return slot;
    }
}
