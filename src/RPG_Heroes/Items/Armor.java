package RPG_Heroes.Items;

import RPG_Heroes.HeroAttribute;

public class Armor extends Item{
    private ArmorType armorType;
    private HeroAttribute armorAttribute; // The Armor class that extends Item and stores armor type and armor attributes
    public Armor(String name, int requiredLevel, ArmorType armorType, Slot slot, HeroAttribute armorAttribute) {
        super(name, requiredLevel, slot);
        this.armorType = armorType;
        this.armorAttribute = armorAttribute;
    }
    public ArmorType getArmorType() {
        return armorType;
    }
    public HeroAttribute getArmorAttribute() {
        return armorAttribute;
    }
}
