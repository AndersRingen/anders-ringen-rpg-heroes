package RPG_Heroes.Items;

public enum Slot { // The different types of slots where equipment can be equipped on the body
    Weapon, Head, Body, Legs
}
