package RPG_Heroes.Items;

public enum WeaponType { // The different types of weapons
    Axes, Bows, Daggers, Hammers, Staffs, Swords, Wands
}
