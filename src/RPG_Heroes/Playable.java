package RPG_Heroes;

import RPG_Heroes.Items.ArmorType;
import RPG_Heroes.Items.Slot;
import RPG_Heroes.Items.WeaponType;

import java.util.List;

public interface Playable { // The interface that contains all the public facing methods of the parent Hero class
    void LevelUp();
    void EquipWeapon(String name, int requiredLevel, WeaponType weaponType, int weaponDamage) throws HerosExceptions;
    void EquipArmor(String name, int requiredLevel, ArmorType armorType, Slot slot, HeroAttribute armorAttribute) throws HerosExceptions;
    float Damage();
    List<Float> TotalAttributes();
    String Display();
}
