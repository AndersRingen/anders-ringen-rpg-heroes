package RPG_Heroes;

public class HerosExceptions extends Exception { // The exceptions that are thrown in EquipWeapon and EquipArmor
    public HerosExceptions(String message) {
        super(message);
    }
}
