package RPG_Heroes;

import java.util.ArrayList;
import java.util.List;

public class HeroAttribute {
    private int strength;
    private int dexterity;
    private int intelligence;

    public HeroAttribute(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;      // The attributes that is used with hero attributes and armor attributes
        this.intelligence = intelligence;
    }

    public void Stronger(){
        strength++;
    }
    public void Faster(){
        dexterity++;
    }
    public void Smarter(){
        intelligence++;
    }
    public int getStrength() {
        return strength;
    }
    public int getDexterity() {
        return dexterity;
    }
    public int getIntelligence() {
        return intelligence;
    }

    public List<Integer> getAllAttributes() { // A method that return all attributes in a list
        List<Integer> allAttributes= new ArrayList<Integer>();
        allAttributes.add(getStrength());     // Used by both hero and armor
        allAttributes.add(getDexterity());
        allAttributes.add(getIntelligence());
        return allAttributes;
    }
}
