package RPG_Heroes.Heroes;

import RPG_Heroes.HeroAttribute;
import RPG_Heroes.HerosExceptions;
import RPG_Heroes.Items.*;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class HeroTest {

    @Test
    public void nr1_createHero_shouldHaveCorrectName() {
        // Arrange
        String name = "Dewald";
        String expected = "Dewald";
        // Act
        Mage mage = new Mage(name);
        //Assert
        assertEquals(expected, mage.getName()); // Had to change variables from private to public?!
    }
    @Test
    public void nr1_createHero_shouldHaveCorrectLevel() {
        // Arrange
        int expected = 1;
        // Act
        Mage mage = new Mage("Dewald");
        //Assert
        assertEquals(expected, mage.getLevel());
    }
    @Test
    public void nr1_createHero_shouldHaveRightAttributes() {
        // Arrange
        List<Integer> expected = Arrays.asList(1,1,8);
        // Act
        Mage mage = new Mage("Dewald");
        //Assert
        assertEquals(expected, mage.heroAttribute.getAllAttributes());
    }

    ////////////////////////////////////////////////////////////////////////

    @Test
    public void nr2_increaseLevel_mage_shouldHaveIncreasedLevel() {
        // Arrange
        int expected = 2;
        // Act
        Mage mage = new Mage("Dewald");
        mage.LevelUp();
        //Assert
        assertEquals(expected, mage.getLevel());
    }
    @Test
    public void nr2_increaseLevel_mage_shouldHaveIncreasedAttributes() {
        // Arrange
        List<Integer> expected = Arrays.asList(2,2,13);
        // Act
        Mage mage = new Mage("Dewald");
        mage.LevelUp();
        //Assert
        assertEquals(expected, mage.heroAttribute.getAllAttributes());
    }

    @Test
    public void nr2_increaseLevel_ranger_shouldHaveIncreasedLevel() {
        // Arrange
        int expected = 2;
        // Act
        Mage mage = new Mage("Dewald");
        mage.LevelUp();
        //Assert
        assertEquals(expected, mage.getLevel());
    }
    @Test
    public void nr2_increaseLevel_ranger_shouldHaveIncreasedAttributes() {
        // Arrange
        List<Integer> expected = Arrays.asList(2,12,2);
        // Act
        Ranger ranger = new Ranger("Dewald");
        ranger.LevelUp();
        //Assert
        assertEquals(expected, ranger.heroAttribute.getAllAttributes());
    }

    @Test
    public void nr2_increaseLevel_rouge_shouldHaveIncreasedLevel() {
        // Arrange
        int expected = 2;
        // Act
        Mage mage = new Mage("Dewald");
        mage.LevelUp();
        //Assert
        assertEquals(expected, mage.getLevel());
    }
    @Test
    public void nr2_increaseLevel_rouge_shouldHaveIncreasedAttributes() {
        // Arrange
        List<Integer> expected = Arrays.asList(3,10,2);
        // Act
        Rogue rogue = new Rogue("Dewald");
        rogue.LevelUp();
        //Assert
        assertEquals(expected, rogue.heroAttribute.getAllAttributes());
    }

    @Test
    public void nr2_increaseLevel_warrior_shouldHaveIncreasedLevel() {
        // Arrange
        int expected = 2;
        // Act
        Mage mage = new Mage("Dewald");
        mage.LevelUp();
        //Assert
        assertEquals(expected, mage.getLevel());
    }
    @Test
    public void nr2_increaseLevel_warrior_shouldHaveIncreasedAttributes() {
        // Arrange
        List<Integer> expected = Arrays.asList(8,4,2);
        // Act
        Warrior warrior = new Warrior("Dewald");
        warrior.LevelUp();
        //Assert
        assertEquals(expected, warrior.heroAttribute.getAllAttributes());
    }

//////////////////////////////////////////////////////////////////////////////////

    @Test
    public void nr3_createWeapon_shouldHaveCorrectName() {
        // Arrange
        String expected = "Slimy Wand";
        int requiredLevel = 2;
        WeaponType weaponType = WeaponType.Wands;
        int weaponDamage = 5;
        // Act
        Weapon weapon = new Weapon("Slimy Wand", requiredLevel, weaponType, weaponDamage);
        //Assert
        assertEquals(expected, weapon.getName());
    }
    @Test
    public void nr3_createWeapon_shouldHaveCorrectRequiredLevel() {
        // Arrange
        int expected = 3;
        String name = "Slimy Wand";
        WeaponType weaponType = WeaponType.Wands;
        int weaponDamage = 5;
        // Act
        Weapon weapon = new Weapon(name, 3, weaponType, weaponDamage);
        //Assert
        assertEquals(expected, weapon.getRequiredLevel());
    }
    @Test
    public void nr3_createWeapon_shouldHaveCorrectSlot() {
        // Arrange
        Slot expected = Slot.Weapon;
        String name = "Slimy Wand";
        int requiredLevel = 2;
        WeaponType weaponType = WeaponType.Wands;
        int weaponDamage = 5;
        // Act
        Weapon weapon = new Weapon(name, requiredLevel, weaponType, weaponDamage);
        //Assert
        assertEquals(expected, weapon.getSlot());
    }
    @Test
    public void nr3_createWeapon_shouldHaveCorrectWeaponType() {
        // Arrange
        WeaponType expected = WeaponType.Wands;
        String name = "Slimy Wand";
        int requiredLevel = 2;
        int weaponDamage = 5;
        // Act
        Weapon weapon = new Weapon(name, requiredLevel, WeaponType.Wands, weaponDamage);
        //Assert
        assertEquals(expected, weapon.getWeaponType());
    }
    @Test
    public void nr3_createWeapon_shouldHaveCorrectDamage() {
        // Arrange
        int expected = 5;
        String name = "Slimy Wand";
        int requiredLevel = 2;
        WeaponType weaponType = WeaponType.Wands;
        // Act
        Weapon weapon = new Weapon(name, requiredLevel, weaponType,5);
        //Assert
        assertEquals(expected, weapon.getWeaponDamage());
    }

    ////////////////////////////////////////////////////////////////////////////////////

    @Test
    public void nr4_createArmor_shouldHaveCorrectName() {
        // Arrange
        String expected = "Toe Breaking Helmet";
        int requiredLevel = 2;
        ArmorType armorType = ArmorType.Plate;
        Slot slot = Slot.Head;
        HeroAttribute armorAttributes = new HeroAttribute(1,2,3);
        // Act
        Armor armor = new Armor("Toe Breaking Helmet", requiredLevel, armorType, slot, armorAttributes);
        //Assert
        assertEquals(expected, armor.getName());
    }
    @Test
    public void nr4_createArmor_shouldHaveCorrectRequiredLevel() {
        // Arrange
        String name = "Toe Breaking Helmet";
        int expected = 2;
        ArmorType armorType = ArmorType.Plate;
        Slot slot = Slot.Head;
        HeroAttribute armorAttributes = new HeroAttribute(1,2,3);
        // Act
        Armor armor = new Armor(name, 2, armorType, slot, armorAttributes);
        //Assert
        assertEquals(expected, armor.getRequiredLevel());
    }
    @Test
    public void nr4_createArmor_shouldHaveCorrectArmorType() {
        // Arrange
        String name = "Toe Breaking Helmet";
        int requiredLevel = 2;
        ArmorType expected = ArmorType.Plate;
        Slot slot = Slot.Head;
        HeroAttribute armorAttributes = new HeroAttribute(1,2,3);
        // Act
        Armor armor = new Armor(name, requiredLevel, ArmorType.Plate, slot, armorAttributes);
        //Assert
        assertEquals(expected, armor.getArmorType());
    }
    @Test
    public void nr4_createArmor_shouldHaveCorrectSlot() {
        // Arrange
        String name = "Toe Breaking Helmet";
        int requiredLevel = 2;
        ArmorType armorType = ArmorType.Plate;
        Slot expected = Slot.Head;
        HeroAttribute armorAttributes = new HeroAttribute(1,2,3);
        // Act
        Armor armor = new Armor(name, requiredLevel, armorType, Slot.Head, armorAttributes);
        //Assert
        assertEquals(expected, armor.getSlot());
    }
    @Test
    public void nr4_createArmor_shouldHaveCorrectArmorAttributes() {
        // Arrange
        String name = "Toe Breaking Helmet";
        int requiredLevel = 2;
        ArmorType armorType = ArmorType.Plate;
        Slot slot = Slot.Head;
        List<Integer> expected = Arrays.asList(1,2,3);
        // Act
        Armor armor = new Armor(name, requiredLevel, armorType, slot, new HeroAttribute(1,2,3));
        //Assert
        assertEquals(expected, armor.getArmorAttribute().getAllAttributes());
    }

    //////////////////////////////////////////////////////////////////////////

    @Test
    public void nr5_equipWeapon_shouldEquipWeapon() throws HerosExceptions {
        // Arrange
        String expected = "Weapon";

        // Act
        Mage mage = new Mage("Dewald");
        mage.EquipWeapon("Slimy Wand", 1, WeaponType.Wands, 5);
        //Assert
        assertEquals(expected, mage.getEquipment().get(Slot.Weapon).getClass().getSimpleName());
    }

    @Test
    public void nr5_equipWeapon_shouldThrowExceptionBecauseOfTooLowLevel() throws HerosExceptions {
        // Arrange
        String expected = "Oops, this did not work! Either the level of the hero is too low or this hero cannot use this weapon type.";
        String actual = "";
        int tooHigh_requiredLevel = 2;
        // Act
        Mage mage = new Mage("Dewald");
        try {
            mage.EquipWeapon("Slimy Wand", tooHigh_requiredLevel, WeaponType.Wands, 5);
        } catch (HerosExceptions e){
            actual = e.getMessage();
        }
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    public void nr5_equipWeapon_shouldThrowExceptionBecauseOfInvalidWeaponType() throws HerosExceptions {
        // Arrange
        String expected = "Oops, this did not work! Either the level of the hero is too low or this hero cannot use this weapon type.";
        String actual = "";
        WeaponType wrong_weaponType = WeaponType.Swords;
        // Act
        Mage mage = new Mage("Dewald");
        try {
            mage.EquipWeapon("Slimy Wand", 1, wrong_weaponType, 5);
        } catch (HerosExceptions e){
            actual = e.getMessage();
        }
        //Assert
        assertEquals(expected, actual);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////

    @Test
    public void nr6_equipArmor_shouldEquipArmor() throws HerosExceptions {
        // Arrange
        String expected = "Weapon";

        // Act
        Mage mage = new Mage("Dewald");
        mage.EquipWeapon("Slimy Wand", 1, WeaponType.Wands, 5);
        //Assert
        assertEquals(expected, mage.getEquipment().get(Slot.Weapon).getClass().getSimpleName());
    }

    @Test
    public void nr6_equipArmor_shouldThrowExceptionBecauseOfTooLowLevel() {
        // Arrange
        String expected = "Oops, this did not work! Either the level of the hero is too low or this hero cannot use this armor type.";
        String actual = "";
        ArmorType wrong_armorType = ArmorType.Mail;
        // Act
        Mage mage = new Mage("Dewald");
        try {
            mage.EquipArmor("Slimy Wand", 1, wrong_armorType, Slot.Body, new HeroAttribute(1,2,3));
        } catch (HerosExceptions e){
            actual = e.getMessage();
        }
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    public void nr6_equipArmor_shouldThrowExceptionBecauseOfInvalidArmorType() {
        // Arrange
        String expected = "Oops, this did not work! Either the level of the hero is too low or this hero cannot use this armor type.";
        String actual = "";
        int tooHigh_requiredLevel = 2;
        // Act
        Mage mage = new Mage("Dewald");
        try {
            mage.EquipArmor("Slimy Wand", tooHigh_requiredLevel, ArmorType.Cloth, Slot.Body, new HeroAttribute(1,2,3));
        } catch (HerosExceptions e){
            actual = e.getMessage();
        }
        //Assert
        assertEquals(expected, actual);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////

    @Test
    public void nr7_totalAttributesWithoutEquipment_shouldReturnCorrectAttributes() {
        // Arrange
        float two = 1+1; // default + levelup
        float thirteen = 8+5;
        List<Float> expected = Arrays.asList(two, two, thirteen);
        // Act
        Mage mage = new Mage("Dewald");
        mage.LevelUp();
        List<Float> actual = mage.TotalAttributes();
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    public void nr7_totalAttributesWithOneArmor_shouldReturnCorrectAttributes() throws HerosExceptions {
        // Arrange
        float three = 1+1+1; // default + levelup + pants
        float four = 1+1+2;
        float sixteen = 8+5+3;
        List<Float> expected = Arrays.asList(three, four, sixteen);
        // Act
        Mage mage = new Mage("Dewald");
        mage.LevelUp();
        mage.EquipArmor("Bulky Pants", 2, ArmorType.Cloth, Slot.Legs, new HeroAttribute(1,2,3));
        List<Float> actual = mage.TotalAttributes();
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    public void nr7_totalAttributesWithTwoArmors_shouldReturnCorrectAttributes() throws HerosExceptions {
        // Arrange
        float six = 1+1+1+3; // default + levelup + pants + body
        float seventeen = 8+5+3+1;
        List<Float> expected = Arrays.asList(six, six, seventeen);
        // Act
        Mage mage = new Mage("Dewald");
        mage.LevelUp();
        mage.EquipArmor("Bulky Pants", 2, ArmorType.Cloth, Slot.Legs, new HeroAttribute(1,2,3));
        mage.EquipArmor("Grey Hoodie", 2, ArmorType.Cloth, Slot.Body, new HeroAttribute(3,2,1));
        List<Float> actual = mage.TotalAttributes();
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    public void nr7_totalAttributesWithReplacedArmor_shouldReturnCorrectAttributes() throws HerosExceptions {
        // Arrange
        float five = 1+1+3;
        float four = 1+1+2;
        float fourteen = 8+5+1;
        List<Float> expected = Arrays.asList(five, four, fourteen);
        // Act
        Mage mage = new Mage("Dewald");
        mage.LevelUp();
        mage.EquipArmor("Bulky Pants", 2, ArmorType.Cloth, Slot.Legs, new HeroAttribute(1,2,3));
        mage.EquipArmor("Pants With A Bigger Bulk?", 2, ArmorType.Cloth, Slot.Legs, new HeroAttribute(3,2,1));
        List<Float> actual = mage.TotalAttributes();
        //Assert
        assertEquals(expected, actual);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Test
    public void nr8_calculateDamageWithNoWeapon_shouldReturn1Damage() {
        // Arrange
        int expected = 1;
        // Act
        Mage mage = new Mage("Dewald");
        //Assert
        assertEquals(expected, mage.Damage());
    }
    @Test
    public void nr8_calculateDamageWithWeapon_shouldReturnCorrectDamage() throws HerosExceptions {
        // Arrange
        float expected = ((5)*(1+((float)8/(float)100))); // The calculations of weaponDamage * (1 + (intelligence/100)
        // Act
        Mage mage = new Mage("Dewald");
        mage.EquipWeapon("Anduril", 1, WeaponType.Wands, 5);
        //Assert
        assertEquals(expected, mage.Damage());
    }
    @Test
    public void nr8_calculateDamageWithReplacedWeapon_shouldReturnCorrectDamage() throws HerosExceptions {
        // Arrange
        float expected = (((float)8)*((float) 1+((float)8/(float) 100))); // The calculations of weaponDamage * (1 + (intelligence/100)
        // Act
        Mage mage = new Mage("Dewald");
        mage.EquipWeapon("Anduril", 1, WeaponType.Wands, 5);
        mage.EquipWeapon("Fire Staff", 1, WeaponType.Staffs, 8); // equip another weapon
        //Assert
        assertEquals(expected, mage.Damage());
    }
    @Test
    public void nr8_calculateDamageWithWeaponAndArmor_shouldReturnCorrectDamage() throws HerosExceptions {
        // Arrange
        float expected = (((float)5)*((float) 1+(((float)8+(float)6)/(float) 100))); // The calculations of weaponDamage * (1 + (default intelligence + armor intelligence/100)
        // Act
        Mage mage = new Mage("Dewald");
        mage.EquipWeapon("Anduril", 1, WeaponType.Wands, 5); // equip weapon
        mage.EquipArmor("Party Hat", 1, ArmorType.Cloth, Slot.Head, new HeroAttribute(1,2,6)); // equip armor
        //Assert
        assertEquals(expected, mage.Damage());
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Test
    public void nr9_displayStats_shouldReturnCorrectStats() throws HerosExceptions {
        // Arrange
        StringBuilder expected = new StringBuilder();
        String name = "Dewald";
        String classy = "Mage";
        int level = 1;
        int strength = 1;
        int dexterity = 1;
        int intelligence = 8;
        float damage = (float)1;
        expected.append("Name: "+name+"\nClass: "+ classy
                +"\nLevel: "+level
                +"\nStrength: "+strength
                +"\nDexterity: "+dexterity
                +"\nIntelligence: "+intelligence
                +"\nDamage: "+damage);
        // Act
        Mage mage = new Mage("Dewald");
        String actual = mage.Display();
        //Assert
        assertEquals(expected.toString(), actual);
    }
}