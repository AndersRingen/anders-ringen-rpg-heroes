<!-- ABOUT THE PROJECT -->
## RPG Heroes

This is a RPG game project that was made with focus on Object Oriented Programming.

### Built With
This is made with Java in the Intellij IDE and Junit for testing.
* [Java](https://www.java.com/en/)
* [Intellij](https://www.jetbrains.com/idea/)
* [Junit](https://junit.org/junit5/)


<!-- USAGE EXAMPLES -->
## Explaination

In this game you are able to make a hero. You can decide what class the hero is going to be by choosing between Mage, Ranger, Rogue and Warrior. The different heros have different hero attributes that increase as the hero levels up. These consists of Strength, Dexterity and Intelligence. Weapon can be equipped, and has a destinct damage value that will increase the Hero's damage. One can also equip armor in the different slots Head, Body and Legs. The armor has destinct attributes, which will increase the already existing hero attributes. All classes has a list of valid weapon and armor types, and cannot equip weapons and armor from other classes, or if the required level of the item higher than the level of the hero. 


## Usage
To play this game you have to clone the repository onto your own local machine and go into the **Main**-file. This file already have initial data, but if you want to do it from scratch you can create a hero by making an object with its class and its name as input. This is done by writing the following command: 
```sh
Mage mage = new Mage("Dewald");
```
To increase the level of the hero you can run the following command(you can do this multiple lines in a row):
```sh
mage.LevelUp();
```
To equip a weapon you have to give a name, required level, weapon type and damage as input. **WeaponType**, **Slot** and **ArmorType** are enums that you can choose valid types from. The command to do so with example data is like this:
```sh
mage.EquipWeapon("Psycho Wand", 1, WeaponType.Wands, 9);
```
To equip armor you have to do the same at the latter method, just with a heroAttribute class for the armor. The command with example data looks like this: 
```sh
mage.EquipArmor("Sorting Hat", 1, ArmorType.Cloth, Slot.Head,
                new HeroAttribute(1,2,3));
```
Lastly you can display all the stats of the hero by using this method:
```sh
mage.Display();
```

Have fun :)
